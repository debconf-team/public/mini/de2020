# MiniDebConf Regensburg Logo

There are two svg files in this directory:

For the 2020 version:
  * mdc-20-logo.svg
  * mdc-20-logo-text2path.svg

For the 2021 version:
  * mdc-21-logo.svg
  * mdc-21-logo-text2path.svg

mdc-2*-logo.svg has a text object, so the text can be changed.
Though, this requires that the logo font is installed on the viewers machine,
otherwise it will not render correctly. Use this one to change
the text for the next (Mini)DebConf in Regensburg ;-)

mdc-2*-logo-text2path.svg has the text object converted to a path object.
That means the text cannot be edited anymore, but it will render even if the font
is not installed. Use this one for prints, posters, t-shirts, slides …

