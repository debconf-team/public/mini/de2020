# MiniDebConf Regensburg Artwork

# Font Sriracha-Regular

Sriracha is a new Thai + Latin handwriting typeface, with an informal loopless + sans serif design.
It has 2 stylistic set alternate glyph designs and intelligent OpenType features to recreate the impression of handwriting. 

Thanks to Pablo Impallari for the initial OpenType handwriting feature development. 

(The font is from Cadson Demak, license is SIL OPEN FONT LICENSE Version 1.1; Sources: https://github.com/cadsondemak/sriracha)

This font is used in the logo and for headlines.

# Font Liberation Sans

The Liberation Sans is available in the Debian package fonts-liberation. 
It is used for all other texts.

# Color

The red of the swirl is the red color of the Regensburg city coat of arms. The RBG-color is #da121a.


